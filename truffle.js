module.exports = {
    // See <http://truffleframework.com/docs/advanced/configuration>
    // to customize your Truffle configuration!
    networks: {
        // This is the development settings when you are using Ganache (see https://github.com/trufflesuite/ganache)
        development: {
            host: "127.0.0.1",
            port: 7545,
            network_id: "5777" // Match any network id
        }
    }
};
