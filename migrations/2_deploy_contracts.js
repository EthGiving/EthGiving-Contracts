var Destructible = artifacts.require("zeppelin-solidity/contracts/lifecycle/Destructible.sol");
var Ownable = artifacts.require("zeppelin-solidity/contracts/ownership/Ownable.sol");
var Authentication = artifacts.require("./Authentication.sol");

module.exports = function(deployer) {
  deployer.deploy(Ownable);
  deployer.link(Ownable, Destructible);
  deployer.deploy(Destructible);
  deployer.link(Destructible, Authentication);
  deployer.deploy(Authentication);
};
