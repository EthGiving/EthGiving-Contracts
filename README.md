# Smart contracts behind EthGiving

This project will store all smart contracts that are used to power EthGiving .

##Basic schema

![](images/basic_architecture_wip.png)

## Technology used

* Ethereum, Solidity
* Truffle
* Ganache

## Directory layout

## Prerequisite


## Getting started

clone this repo

npm install in main directory

## Testing

* truffle test

## License

MIT